# Renvooiservice 

Maart 2023 is een nieuwe versie van de RenvooiService API en de Testpagina uitgerold met nieuwe URL's. De oude URL's blijven na de uitrol tijdelijk bruikbaar, echter deze leiden naar de oude versie van de software. Alleen via de nieuwe URL is de uitgerolde nieuwe software te benaderen.

**RenvooiService API nieuw**: 
- https://renvooiservice-eto.overheid.nl/regelingmutatie-maak
- de documentatie is in de API zelf opgenomen:
-- OpenAPI JSON documentatie via de root: https://renvooiservice-eto.overheid.nl
-- PDF met functionele documentatie via endpoint: https://renvooiservice-eto.overheid.nl/documentatie

**Testpagina nieuw**: https://renvooiservice-eto.overheid.nl/test